public class Monom {
    private double coeficient;
    private int putere;
    public Monom(double coeficient, int putere) {
        this.coeficient = coeficient;
        this.putere = putere;
    }
    public Monom(){}
    public double getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(double coeficient) {
        this.coeficient = coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    public void inmultire(Monom a){
        this.coeficient*=a.coeficient;
        this.putere+=a.putere;
    }

    public void impartire(Monom a){
        this.coeficient/=a.coeficient;
        this.putere-=a.putere;
    }

    public void derivare(){
        this.coeficient*=this.putere;
        this.putere--;
    }

    public void integrare(){
        this.coeficient/=(this.putere+1);
        this.putere++;
    }

    public void adunare(Monom a){
        this.coeficient+=a.coeficient;
    }

    public void scadere(Monom a){
        this.coeficient-=a.coeficient;
    }

    @Override
    public String toString(){
        if(this.coeficient==0){
            return "";
        } else if(this.putere==0) {
            if(this.coeficient==Math.ceil(this.coeficient)) {return String.valueOf((int)this.coeficient);}
            else {return String.valueOf(this.coeficient);}
        } else if(this.coeficient==1) {
            if(this.putere==1) { return "x"; }
            else { return "x^" + this.putere; }
        } else if(this.coeficient==-1) {
            if(this.putere==1) { return "-x"; }
            else { return "-x^" + this.putere; }
        } else if(this.putere==1) {
            if(this.coeficient==Math.ceil(this.coeficient)) { return (int)this.coeficient + "x";}
            else { return this.coeficient + "x";}
        } else if(this.coeficient==Math.ceil(this.coeficient)) { return (int)this.coeficient + "x^" + this.putere;
        } else { return this.coeficient + "x^" + this.putere;}
    }
}
