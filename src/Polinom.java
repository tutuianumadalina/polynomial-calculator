import java.util.ArrayList;
import java.util.List;

public class Polinom {
    public List<Monom> monom;
    public void add(Monom a){
        this.monom.add(a);
    }
    public Monom grad(){
        Monom g=new Monom(0.0,0);
        for(Monom it:this.monom){
            if(it.getPutere()>g.getPutere()){
                g=it;
            }
        }
        return g;
    }
    public Polinom(){
        monom = new ArrayList<>();
    }
    public Polinom(List<Monom> monom){
        monom = monom;
    }
    public List<Monom> getPolinom() {
        return monom;
    }
    @Override
    public String toString() {
        String s = "";
        for(Monom m:monom){
            if(m.getCoeficient()>=0){
                s += "+";
            }
            s += m.toString();
        }
        if(s.charAt(0)=='+')
            s=s.substring(1);
        return s;
    }

    public void setMonom(List<Monom> polinom) {
        this.monom=polinom;
    }
}
