

public class Main {
    public static void main(String[] args) {
        Model model = new Model();
        Interfata view = new Interfata(model);
        Controller controller = new Controller(model, view);
    }
}
