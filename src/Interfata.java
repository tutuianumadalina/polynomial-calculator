
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interfata {
    private JTextField polinomialCalculatorTextField;
    private JTextField polinom1TextField;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField polinom2TextField;
    private JTextField textField6;
    private JButton impartireButton;
    private JButton derivarePolinom1Button;
    private JButton integrarePolinom2Button;
    private JButton inmultireButton;
    private JPanel panel;
    private JButton adunareButton;
    private JButton scadereButton;

    public Interfata(Model model) {
        JFrame frame=null;
        initialize(frame);
    }

    public void initialize(JFrame frame) {
        if(frame==null){
            frame = new JFrame("Calculator polinomial");
            frame.setContentPane(panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        Dimension a=new Dimension();
        a.height=768;
        a.width=1366;
        frame.setMinimumSize(a);
        frame.pack();
        frame.setVisible(true);

    }
    void ListenerDerivare(ActionListener derivare){
        derivarePolinom1Button.addActionListener(derivare);
    }
    void ListenerIntegrare(ActionListener integrare){
        integrarePolinom2Button.addActionListener(integrare);
    }
    void ListenerAdunare(ActionListener adunare){
        adunareButton.addActionListener(adunare);
    }
    void ListenerScadere(ActionListener scadere){
        scadereButton.addActionListener(scadere);
    }
    void ListenerInmultire(ActionListener inmultire){
        inmultireButton.addActionListener(inmultire);
    }
    void ListenerImpartire(ActionListener impartire){
        impartireButton.addActionListener(impartire);
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public JTextField getTextField6() {
        return textField6;
    }

    public void setTextField2(String text) {
        this.textField2.setText(text);
    }
}