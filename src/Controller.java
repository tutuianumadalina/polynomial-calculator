import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Model model;
    private Interfata view;
    Controller(Model model, Interfata view){
        this.model=model;
        this.view=view;
        view.ListenerDerivare(new Derivare());
        view.ListenerIntegrare(new Integrare());
        view.ListenerAdunare(new Adunare());
        view.ListenerScadere(new Scadere());
        view.ListenerInmultire(new Inmultire());
        view.ListenerImpartire(new Impartire());
    }
    class Derivare implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP3 = "";
            userInputP3 = view.getTextField3().getText();
            Polinom p3 = new Polinom();
            if (userInputP3.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus polinomul");
                return;
            }
            p3 = getPolinom(userInputP3);
            Polinom result = new Polinom();
            for(Monom m : p3.getPolinom()){
                if(m.getPutere()!=0)
                {
                    int exp;
                    double coef;
                    coef = m.getCoeficient() * m.getPutere();
                    exp = m.getPutere() - 1;
                    Monom aux = new Monom(coef,exp);
                    if(aux.getCoeficient()!=0)
                        result.add(aux);
                }
            }
            Collections.sort(result.getPolinom(), new SortExp());
            view.setTextField2(result.toString());
        }
    }
    class Integrare implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP3 = "";
            userInputP3 = view.getTextField3().getText();
            Polinom p3 = new Polinom();
            if (userInputP3.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus polinomul");
                return;
            }
            System.out.println(userInputP3);
            p3 = getPolinom(userInputP3);
            if(p3.getPolinom().get(0).getPutere()==0 && p3.getPolinom().get(0).getCoeficient()==0)
            {
                view.setTextField2("0");
                return;
            }
            Polinom result = new Polinom();
            for(Monom m : p3.getPolinom()){
                if(m.getPutere()==0){
                    Monom aux = new Monom(m.getCoeficient(),1);
                    result.add(aux);
                }else {
                    int exp;
                    double coef;
                    coef = m.getCoeficient()/(m.getPutere()+1);
                    exp = m.getPutere() + 1;
                    Monom aux = new Monom(coef,exp);
                    result.add(aux);
                }
            }
            Collections.sort(result.getPolinom(), new SortExp());
            view.setTextField2(result.toString());
        }
    }
    class Adunare implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP1 = "";
            String userInputP2 = "";
            userInputP1 = view.getTextField3().getText();
            userInputP2 = view.getTextField6().getText();
            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            if (userInputP1.isEmpty() || userInputP2.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus unul dintre polinoame");
                return;
            }
            p1 = getPolinom(userInputP1);
            p2 = getPolinom(userInputP2);
            Polinom result = new Polinom();
            for(Monom m1 : p1.getPolinom()){
                result.add(m1);
            }
            for(Monom m2 : p2.getPolinom()){
                result.add(m2);
            }
            for(Monom m1: p1.getPolinom())
            {
                for(Monom m2 : p2.getPolinom()){
                    if(m1.getPutere()==m2.getPutere())
                    {
                        double aux;
                        aux = m1.getCoeficient() + m2.getCoeficient();
                        Monom m = new Monom(aux,m1.getPutere());
                        result.getPolinom().remove(m1);
                        result.getPolinom().remove(m2);
                        if(m.getCoeficient()!=0)
                        {result.add(m);}
                    }
                }
            }
            Collections.sort(result.getPolinom(), new SortExp());
            view.setTextField2(result.toString());
        }
    }
    class Scadere implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP1 = "";
            String userInputP2 = "";
            userInputP1 = view.getTextField3().getText();
            userInputP2 = view.getTextField6().getText();
            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            if (userInputP1.isEmpty() || userInputP2.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus unul dintre polinoame");
                return;
            }
            p1 = getPolinom(userInputP1);
            p2 = getPolinom(userInputP2);
            Polinom result = new Polinom();
            for(Monom m1 : p1.getPolinom()){
                result.add(m1);
            }
            for(Monom m2 : p2.getPolinom()){
                Monom aux = new Monom();
                aux.setCoeficient(-m2.getCoeficient());
                aux.setPutere(m2.getPutere());
                result.add(aux);
            }
            for(Monom m1: p1.getPolinom())
            {
                for(Monom m2 : p2.getPolinom()){
                    if(m1.getPutere()==m2.getPutere())
                    {
                        double aux;
                        aux = m1.getCoeficient() - m2.getCoeficient();
                        Monom m = new Monom(aux,m1.getPutere());
                        result.getPolinom().remove(m1);
                        result.getPolinom().remove(m2);
                        if(m.getCoeficient()!=0)
                        {result.add(m);}
                    }
                }
            }
            Collections.sort(result.getPolinom(), new SortExp());
            view.setTextField2(result.toString());
        }
    }
    class Impartire implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP1 = "";
            String userInputP2 = "";
            userInputP1 = view.getTextField3().getText();
            userInputP2 = view.getTextField6().getText();
            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            Polinom result = new Polinom();
            if (userInputP1.isEmpty() || userInputP2.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus unul dintre polinoame");
                view.setTextField2("");
                return;
            }
            p1 = getPolinom(userInputP1);
            p2 = getPolinom(userInputP2);
            if(p1.getPolinom().get(0).getPutere()<p2.getPolinom().get(0).getPutere())
            {
                JOptionPane.showMessageDialog(null, "Nu se poate imparti.");
                view.setTextField2("");
                return;
            }
            if(p2.getPolinom().get(0).getCoeficient()==0&&p2.getPolinom().get(0).getPutere()==0)
            {
                JOptionPane.showMessageDialog(null, "Nu se poate imparti cu 0.");
                view.setTextField2("");
                return;
            }

            Polinom copyP1 = p1;
            Polinom rest = new Polinom();
            Polinom aux = new Polinom();
            while((!copyP1.getPolinom().isEmpty())&&copyP1.getPolinom().get(0).getPutere()>=p2.getPolinom().get(0).getPutere()) {
                int auxExp;
                double auxCoef;
                auxCoef = copyP1.getPolinom().get(0).getCoeficient() / p2.getPolinom().get(0).getCoeficient();
                auxExp = copyP1.getPolinom().get(0).getPutere() - p2.getPolinom().get(0).getPutere();
                Monom m = new Monom(auxCoef,auxExp);
                Monom m_aux = new Monom(m.getCoeficient(),m.getPutere());
                result.add(m);
                double coef = m_aux.getCoeficient();
                m_aux.setCoeficient(-coef);
                aux.add(m_aux);
                Polinom aux2 = inmultire(aux, p2);
                Polinom deepCopy = adunare(copyP1,aux2);
                copyP1.setMonom(deepCopy.getPolinom());
                aux.getPolinom().remove(m_aux);
                rest.setMonom(copyP1.getPolinom());
            }


            if(!(rest.getPolinom().isEmpty()))
                view.setTextField2("Impartitor: " + result.toString() + " -- Rest: " + rest);
            else
                view.setTextField2("Impartitor: " + result.toString() + " -- Rest: 0");
        }
    }
    class Inmultire implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String userInputP1 = "";
            String userInputP2 = "";
            userInputP1 = view.getTextField3().getText();
            userInputP2 = view.getTextField6().getText();
            Polinom p1 = new Polinom();
            Polinom p2 = new Polinom();
            if (userInputP1.isEmpty() || userInputP2.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nu ai introdus unul dintre polinoame");
                return;
            }
            p1 = getPolinom(userInputP1);
            p2 = getPolinom(userInputP2);
            Polinom result = new Polinom();
            for(Monom m1 : p1.getPolinom()){
                result.add(m1);
            }
            for(Monom m2 : p2.getPolinom()){
                result.add(m2);
            }
            for(Monom m1: p1.getPolinom())
            {
                for(Monom m2 : p2.getPolinom()){
                    int aux_putere;
                    double aux_coeficient;
                    aux_putere = m1.getPutere() + m2.getPutere();
                    aux_coeficient = m1.getCoeficient() * m2.getCoeficient();
                    Monom m = new Monom(aux_coeficient, aux_putere);
                    result.getPolinom().remove(m1);
                    result.getPolinom().remove(m2);
                    if(m.getCoeficient()!=0)
                    {result.add(m);}
                }
            }
            Collections.sort(result.getPolinom(), new SortExp());
            view.setTextField2(result.toString());
        }
    }



    static class SortExp implements Comparator<Monom>
    {
        public int compare(Monom a, Monom b)
        {
            Integer exp1 = a.getPutere();
            Integer exp2 = b.getPutere();
            return exp2.compareTo(exp1);
        }
    }

    public static Polinom getPolinom(String p1) {
        String monomialPattern = "([-+]?)(\\d*\\.?\\d*)?([xX](\\^-?\\d*\\.?\\d*)?)?";
        Pattern pattern = Pattern.compile(monomialPattern);
        String pol1 = p1;
        pol1 = pol1.replaceAll("\\s+", "");
        Matcher matcher = pattern.matcher(pol1);
        Monom m;
        Polinom p = new Polinom();
        while (matcher.find()) {
            String g1 = matcher.group(1);
            String g2 = matcher.group(2);
            String g3 = matcher.group(3);
            String g4 = matcher.group(4);
            int coeff, exp;
            try {
                if (g1.isEmpty() && g2.isEmpty() && g3.equals(null) && g4.equals(null))
                    break;
                else {
                    if (g2.isEmpty()) {
                        if (g1.equals("-")) {
                            coeff = -1;
                        } else {
                            coeff = 1;
                        }
                    } else {
                        if (g1.equals("-"))
                            coeff = -Integer.parseInt(g2);
                        else
                            coeff = Integer.parseInt(g2);
                    }
                    String xp = "";
                    if (g4 == null) {
                        if (g3 == null)
                            exp = 0;
                        else
                            exp = 1;
                    } else {
                        for (int i = 1; i < g4.length(); i++) {
                            xp += g4.charAt(i);
                        }
                        exp = Integer.parseInt(xp);
                    }
                }
                m = new Monom(coeff, exp);
                p.add(m);
            } catch (NullPointerException ex) {

            }

        }
        return p;
    }

    public static Polinom adunare(Polinom p1 , Polinom p2) {
        Polinom result = new Polinom();
        for(Monom m1 : p1.getPolinom()){
            result.add(m1);
        }
        for(Monom m2 : p2.getPolinom()){
            result.add(m2);
        }
        for(Monom m1: p1.getPolinom())
        {
            for(Monom m2 : p2.getPolinom()){
                if(m1.getPutere()==m2.getPutere())
                {
                    double aux;
                    aux = m1.getCoeficient() + m2.getCoeficient();
                    Monom m = new Monom(aux,m1.getPutere());
                    result.getPolinom().remove(m1);
                    result.getPolinom().remove(m2);
                    if(m.getCoeficient()!=0)
                    {result.add(m);}
                }
            }
        }
        Collections.sort(result.getPolinom(), new SortExp());
        return result;
    }

    public static Polinom inmultire(Polinom p1, Polinom p2){
        Polinom result = new Polinom();
        for(Monom m1 : p1.getPolinom())
            for(Monom m2 : p2.getPolinom()) {
                int auxExp;
                double auxCoef;
                auxCoef = m1.getCoeficient() * m2.getCoeficient();
                auxExp = m1.getPutere() + m2.getPutere();
                Monom m = new Monom(auxCoef,auxExp);
                result.add(m);
            }
        for(int i=0 ; i< result.getPolinom().size()-1; i++)
            for(int j=i+1 ; j < result.getPolinom().size() ; j++) {
                Monom m1 = result.getPolinom().get(i);
                Monom m2 = result.getPolinom().get(j);
                if (m1.getPutere() == m2.getPutere()) {
                    double aux;
                    aux = m1.getCoeficient() + m2.getCoeficient();
                    Monom m = new Monom(aux,m1.getPutere());
                    result.getPolinom().remove(m1);
                    result.getPolinom().remove(m2);
                    if(m.getCoeficient()!=0) {
                        result.add(m);
                    }
                    i=0;
                }
            }
        Collections.sort(result.getPolinom(), new SortExp());
        return result;
    }
}
